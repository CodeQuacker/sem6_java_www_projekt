package com.sem6_pawww_projekt.storageapp.hexagon.application

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.*
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.EMPTY_CATEGORY
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.ItemProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserRoles
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.CategoryPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.ItemPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.LogPersistencePort
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.provisioning.UserDetailsManager
import java.lang.Boolean
import java.util.*
import java.util.List

internal class StorageAppServiceTest {
    @Mock
    var categoryPersistencePort: CategoryPersistencePort? = null

    @Mock
    var itemPersistencePort: ItemPersistencePort? = null

    @Mock
    var userDetailsManager: UserDetailsManager? = null

    @Mock
    var passwordEncoder: PasswordEncoder? = null

    @Mock
    var logPersistencePort: LogPersistencePort? = null

    @InjectMocks
    var storageAppService: StorageAppService? = null
    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testGetAllCategories() {
        val uuid= UUID.randomUUID()
        Mockito.`when`(categoryPersistencePort!!.getAllCategories()).thenReturn(
            List.of(
                CategoryDto(
                    uuid,
                    "name",
                    null
                )
            )
        )
        val result = storageAppService!!.getAllCategories()
        Assertions.assertEquals(List.of(CategoryProjection(uuid, "name", null)), result)
    }

    @Test
    fun testGetAllFilteredCategories() {
        val uuid= UUID.randomUUID()
        Mockito.`when`(categoryPersistencePort!!.getAllCategories()).thenReturn(
            List.of(
                CategoryDto(
                    uuid,
                    "name",
                    null
                ),
                CategoryDto(
                    UUID.randomUUID(),
                    "subCategoryName",
                    CategoryDto(uuid, "name", null)
                )
            )
        )
        val result = storageAppService!!.getAllFilteredCategories(
            FilterCategoriesCommand(
                "filterTerm",
                true,
                "masterCategoryNameFilter"
            )
        )
        Assertions.assertEquals(List.of(CategoryProjection(uuid, "name", null)), result)
    }

    @Test
    fun testCreateNewCategory() {
        storageAppService!!.createNewCategory(CreateCategoryCommand("name", "masterCategoryName"))
    }

    @Test
    fun testDeleteCategory() {
        storageAppService!!.deleteCategory(DeleteCategoryCommand(UUID.randomUUID()))
    }

    @Test
    fun testGetAllItems() {
        val uuid = UUID.randomUUID()
        Mockito.`when`(itemPersistencePort!!.getAllItems()).thenReturn(
            List.of(
                ItemDto(
                    uuid,
                    "name",
                    0.0,
                    0,
                    category = CategoryDto(
                        UUID.randomUUID(),
                        "category",
                        null
                    )
                )
            )
        )
        val result = storageAppService!!.getAllItems()
        Assertions.assertEquals(List.of(ItemProjection(uuid, "name", 0.0, 0, "category")), result)
    }

    @Test
    fun testGetAllFilteredItems() {
        val uuid = UUID.randomUUID()
        Mockito.`when`(itemPersistencePort!!.getAllItems()).thenReturn(
            listOf(
                ItemDto(
                    uuid,
                    "name",
                    0.0,
                    0,
                    category = CategoryDto(
                        UUID.randomUUID(),
                        "category",
                        null
                    )
                ),
                ItemDto(
                    uuid,
                    "name2",
                    2.0,
                    10,
                    category = CategoryDto(
                        UUID.randomUUID(),
                        "category2",
                        null
                    )
                )
            )
        )
        val result = storageAppService!!.getAllFilteredItems(FilterItemsCommand(categoryIdFilter = EMPTY_CATEGORY.toString()))
        Assertions.assertEquals(listOf(ItemProjection(uuid, "name2", 2.0, 10, "category2")), result)
    }

    @Test
    fun testGetItem() {
        val uuid = UUID.randomUUID()
        Mockito.`when`(itemPersistencePort!!.getItem(uuid)).thenReturn(
            ItemDto(
                uuid,
                "name",
                0.0,
                0,
                category = CategoryDto(
                    UUID.randomUUID(),
                    "category",
                    null
                )
            )
        )
        val result = storageAppService!!.getItem(GetItemCommand(id = uuid))
        Assertions.assertEquals(ItemProjection(uuid, "name", 0.0, 0, "category"), result)
    }

    @Test
    fun testCreateNewItem() {
        storageAppService!!.createNewItem(CreateItemCommand("name", 1.0, 10, UUID.randomUUID().toString()))
    }

    @Test
    fun testEditItem() {
        storageAppService!!.editItem(EditItemCommand(UUID.randomUUID(), "name", 1.0, 10, UUID.randomUUID().toString()))
    }

    @Test
    fun testDeleteItem() {
        storageAppService!!.deleteItem(DeleteItemCommand(UUID.randomUUID()))
    }

    @Test
    fun testRegisterUser() {
        Mockito.`when`(userDetailsManager!!.userExists("username")).thenReturn(Boolean.FALSE)
        Mockito.`when`(passwordEncoder!!.encode("password")).thenReturn("password")
        storageAppService!!.registerUser(RegisterUserCommand(username = "username", password = "password",role= UserRoles.ADMIN))
    }
}