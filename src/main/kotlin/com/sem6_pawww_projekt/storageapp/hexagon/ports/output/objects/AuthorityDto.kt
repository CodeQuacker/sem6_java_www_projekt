package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects

data class AuthorityDto(
    val username: String,
    val authority: UserRoles
)
