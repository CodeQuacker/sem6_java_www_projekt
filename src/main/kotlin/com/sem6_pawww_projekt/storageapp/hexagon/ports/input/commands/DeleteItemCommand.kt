package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands


import java.util.*

data class DeleteItemCommand(
    val id: UUID
)
