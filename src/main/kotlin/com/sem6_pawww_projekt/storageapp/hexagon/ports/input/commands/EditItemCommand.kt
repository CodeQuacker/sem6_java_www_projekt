package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands


import java.util.*

data class EditItemCommand(
    val id: UUID,
    val name: String,
    val value: Double,
    val amount: Int,
    val category: String,
)
