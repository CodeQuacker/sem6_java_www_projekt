package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.utils

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.AuthorityEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.CategoryEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.ItemEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.LogEntity
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.*

fun CategoryEntity.toDto(): CategoryDto = CategoryDto(
    id = id ?: throw IllegalStateException("CategoryEntity id is null"),
    name = name ?: throw IllegalStateException("CategoryEntity name is null"),
    masterCategory = masterCategory?.toDto()
)

fun ItemEntity.toDto(): ItemDto = ItemDto(
    id = id ?: throw IllegalStateException("ItemEntity id is null"),
    name = name ?: throw IllegalStateException("ItemEntity name is null"),
    value = value ?: throw IllegalStateException("ItemEntity value is null"),
    amount = amount ?: throw IllegalStateException("ItemEntity amount is null"),
    category = category?.toDto() ?: throw IllegalStateException("ItemEntity category is null")
)

fun LogEntity.toDto(): LogDto = LogDto(
    log = log ?: throw IllegalStateException("LogEntity log is null"),
    username = username ?: throw IllegalStateException("LogEntity username is null"),
    timeStamp = timeStamp ?: throw IllegalStateException("LogEntity timeStamp is null"),
)

fun AuthorityEntity.toDto(): AuthorityDto = AuthorityDto(
    username = id?.username?.username ?: throw IllegalStateException("AuthorityEntity username is null"),
    authority = authority?.toUserRole() ?: throw IllegalStateException("AuthorityEntity authority is null")
)

private fun String.toUserRole(): UserRoles = when (this) {
    "ROLE_ADMIN" -> UserRoles.ADMIN
    "ROLE_REGULAR_USER" -> UserRoles.REGULAR_USER
    "ROLE_FULL_USER" -> UserRoles.FULL_USER
    else -> throw IllegalStateException("Unknown authority: $this")
}