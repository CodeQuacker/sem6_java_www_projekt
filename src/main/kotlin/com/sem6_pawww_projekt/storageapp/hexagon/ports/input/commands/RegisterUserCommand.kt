package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserRoles
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull

data class RegisterUserCommand(
    @NotEmpty
    @NotNull
    val username: String,

    @NotEmpty
    @NotNull
    val password: String,

    @NotEmpty
    @NotNull
    val role: UserRoles
)
