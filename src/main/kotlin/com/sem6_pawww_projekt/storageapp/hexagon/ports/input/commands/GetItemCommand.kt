package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands


import java.util.*

data class GetItemCommand(
    val id: UUID
)
