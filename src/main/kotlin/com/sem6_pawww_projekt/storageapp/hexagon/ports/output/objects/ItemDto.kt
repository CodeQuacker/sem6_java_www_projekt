package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.CategoryEntity
import java.util.*

data class ItemDto(
    val id: UUID? = null,
    val name: String,
    val value: Double,
    val amount: Int,
    val category: CategoryDto?
)

