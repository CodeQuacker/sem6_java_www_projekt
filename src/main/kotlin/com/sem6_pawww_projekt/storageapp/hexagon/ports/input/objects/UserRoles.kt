package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects

enum class UserRoles(val label: String) {
    REGULAR_USER("Regular User"),
    FULL_USER("Full User"),
    ADMIN("Administrator"),
}
