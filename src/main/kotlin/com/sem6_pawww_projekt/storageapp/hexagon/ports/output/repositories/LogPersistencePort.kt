package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.LogDto

interface LogPersistencePort {
    fun createLog(logDto: LogDto)
    fun getLogs(): List<LogDto>
}