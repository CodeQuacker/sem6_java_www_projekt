package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto
import java.util.*

interface ItemPersistencePort{
    fun getItem(id: UUID): ItemDto

    fun getAllItems(): List<ItemDto>

    fun saveItem(item: ItemDto)

    fun deleteItem(id: UUID)

}