package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

data class LogStackTraceCommand(
    val stackTrace: String,
    val username: String,
)
