package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import java.util.*

interface CategoryPersistencePort{
    fun createMasterCategory(name: String)

    fun createSubCategory(name: String, masterCategoryName: String)

    fun getAllCategories(): List<CategoryDto>

    fun getMasterCategories(): List<CategoryDto>

    fun getSubCategories(): List<CategoryDto>

    fun getSubCategories(masterCategoryName: String): List<CategoryDto>

    fun deleteCategory(id: UUID)

    fun existsCategory(name: String): Boolean

    fun getCategory(name: String): CategoryDto

    fun getCategoryById(id: UUID): CategoryDto

    fun updateCategory(name: String, newName: String)

    fun updateCategory(name: String, newName: String, masterCategoryName: String)

}