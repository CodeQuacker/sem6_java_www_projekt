package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.AuthorityDto

interface AuthorityPersistencePort {
    fun getAllAuthorities(): List<AuthorityDto>
}
