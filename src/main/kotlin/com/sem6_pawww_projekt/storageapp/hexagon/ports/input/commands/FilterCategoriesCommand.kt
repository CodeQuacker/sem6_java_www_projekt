package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

data class FilterCategoriesCommand(
    val filterTerm: String? = null,
    val onlyMasterCategories: Boolean? = null,
    val masterCategoryNameFilter: String? = null
)
