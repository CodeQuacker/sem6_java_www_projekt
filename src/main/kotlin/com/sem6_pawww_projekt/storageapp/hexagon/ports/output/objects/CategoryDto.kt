package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects

import java.util.UUID

data class CategoryDto(
    val id: UUID,
    val name: String,
    val masterCategory: CategoryDto?
)
