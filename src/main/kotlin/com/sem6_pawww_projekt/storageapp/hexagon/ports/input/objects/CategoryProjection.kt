package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects

import java.util.*

val EMPTY_CATEGORY: UUID = UUID.randomUUID()
data class CategoryProjection(val id: UUID, val name: String, val masterCategory: String?)

data class CategoryTreeProjection(val name: String, val subCategories: List<CategoryTreeProjection>)
