package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

data class CreateItemCommand(
    val name: String,
    val value: Double,
    val amount: Int,
    val category: String,
)
