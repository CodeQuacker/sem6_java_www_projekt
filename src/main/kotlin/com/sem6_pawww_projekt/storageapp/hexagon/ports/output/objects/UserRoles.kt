package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects

enum class UserRoles(val label: String) {
    ADMIN("Administrator"), REGULAR_USER("Regular User"), FULL_USER("Full User")
}
