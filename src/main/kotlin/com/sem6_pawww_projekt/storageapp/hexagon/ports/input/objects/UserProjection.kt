package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects

data class UserProjection(
    val username: String,
    val role: String
)
