package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

data class CreateCategoryCommand(val name: String, val masterCategoryName: String? = null)
