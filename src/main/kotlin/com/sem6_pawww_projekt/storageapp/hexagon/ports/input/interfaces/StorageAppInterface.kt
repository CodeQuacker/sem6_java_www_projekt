package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.*
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.ItemProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserProjection

interface StorageAppInterface {
    fun getAllCategories(): List<CategoryProjection>
    fun createNewCategory(command: CreateCategoryCommand)
    fun deleteCategory(command: DeleteCategoryCommand)
    fun getAllItems(): List<ItemProjection>
    fun createNewItem(command: CreateItemCommand)
    fun editItem(command: EditItemCommand)
    fun deleteItem(command: DeleteItemCommand)

    fun getItem(command: GetItemCommand): ItemProjection
    fun registerUser(command: RegisterUserCommand)
    fun getAllFilteredCategories(filterCategoriesCommand: FilterCategoriesCommand): List<CategoryProjection>
    fun getAllFilteredItems(filterItemsCommand: FilterItemsCommand): List<ItemProjection>
    fun getAllUsers(): List<UserProjection>
    fun logStackTrace(logStackTraceCommand: LogStackTraceCommand)
}