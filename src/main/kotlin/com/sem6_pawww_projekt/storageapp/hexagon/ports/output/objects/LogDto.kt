package com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects

import java.time.LocalDateTime

data class LogDto(
    val log: String,
    val username: String,
    val timeStamp: LocalDateTime?
)
