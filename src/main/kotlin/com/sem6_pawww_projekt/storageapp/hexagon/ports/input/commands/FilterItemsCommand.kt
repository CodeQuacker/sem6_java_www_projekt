package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands

data class FilterItemsCommand(
    val termFilter: String? = null,
    val categoryIdFilter: String? = null,
    val lowerThanValueFilter: Number? = null,
    val greaterThanValueFilter: Number? = null,
    val lowerThanAmountFilter: Number? = null,
    val greaterThanAmountFilter: Number? = null,
)
