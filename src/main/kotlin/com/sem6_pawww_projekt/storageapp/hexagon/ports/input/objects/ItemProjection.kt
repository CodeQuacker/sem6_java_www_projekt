package com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects

import java.util.*

data class ItemProjection(
    val id: UUID,
    val name: String,
    val value: Double,
    val amount: Int,
    val category: String
)
