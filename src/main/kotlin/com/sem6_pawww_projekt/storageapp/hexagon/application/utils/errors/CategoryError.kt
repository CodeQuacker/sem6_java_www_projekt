package com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors

sealed class CategoryError : Exception() {
    object CategoryExists : CategoryError()
}