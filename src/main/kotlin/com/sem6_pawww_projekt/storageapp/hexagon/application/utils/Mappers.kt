package com.sem6_pawww_projekt.storageapp.hexagon.application.utils

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.ItemProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.AuthorityDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto

fun CategoryDto.toProjection(): CategoryProjection = CategoryProjection(
    id = id,
    name = name,
    masterCategory = masterCategory?.name
)

fun ItemDto.toProjection(): ItemProjection = ItemProjection(
    id = id ?: throw IllegalStateException("ItemDto id is null"),
    name = name,
    value = value,
    amount = amount,
    category = category?.name ?: throw IllegalStateException("ItemDto category is null"),
)

fun AuthorityDto.toProjection(): UserProjection = UserProjection(
    username = username,
    role = authority.label
)