package com.sem6_pawww_projekt.storageapp.hexagon.application

import com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors.CategoryError
import com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors.validate
import com.sem6_pawww_projekt.storageapp.hexagon.application.utils.toProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.*
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.EMPTY_CATEGORY
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.ItemProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.LogDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.AuthorityPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.CategoryPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.ItemPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.LogPersistencePort
import org.springframework.security.core.userdetails.User
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.provisioning.UserDetailsManager
import java.time.LocalDateTime
import java.util.*

class StorageAppService(
    private val categoryPersistencePort: CategoryPersistencePort,
    private val itemPersistencePort: ItemPersistencePort,
    private val userDetailsManager: UserDetailsManager,
    private val passwordEncoder: PasswordEncoder,
    private val logPersistencePort: LogPersistencePort,
    private val authorityPersistencePort: AuthorityPersistencePort,
) : StorageAppInterface {

    override fun getAllCategories(): List<CategoryProjection> {
        return categoryPersistencePort.getAllCategories().map { it.toProjection() }

    }

    override fun getAllFilteredCategories(filterCategoriesCommand: FilterCategoriesCommand): List<CategoryProjection> {
        val allCategories = categoryPersistencePort.getAllCategories()
        return when {
            filterCategoriesCommand.onlyMasterCategories == true -> {
                allCategories.filter { it.masterCategory == null }
                    .map { it.toProjection() }
            }

            filterCategoriesCommand.masterCategoryNameFilter != null -> {
                allCategories
                    .filter { it.masterCategory?.name == filterCategoriesCommand.masterCategoryNameFilter }
                    .map { it.toProjection() }
            }

            filterCategoriesCommand.filterTerm != null -> {
                allCategories.filter {
                    it.name.contains(
                        filterCategoriesCommand.filterTerm,
                        ignoreCase = true
                    )
                }
                    .map { it.toProjection() }
            }

            else -> {
                allCategories.map { it.toProjection() }
            }
        }
    }

    override fun createNewCategory(command: CreateCategoryCommand) {
        categoryPersistencePort.run {
            when {
                existsCategory(command.name) -> {
                    throw CategoryError.CategoryExists
                }

                (command.masterCategoryName != null) && existsCategory(command.masterCategoryName) -> {
                    createSubCategory(command.name, command.masterCategoryName)
                }

                else -> {
                    createMasterCategory(command.name)
                }
            }
        }
    }

    override fun deleteCategory(command: DeleteCategoryCommand) {
        categoryPersistencePort.deleteCategory(command.id)
    }

    override fun getAllItems(): List<ItemProjection> {
        return itemPersistencePort.getAllItems().map { it.toProjection() }
    }

    override fun getAllFilteredItems(filterItemsCommand: FilterItemsCommand): List<ItemProjection> {
        val categoryIdsForFilteringList = filterItemsCommand.categoryIdFilter?.let { filterCategory ->
            categoryPersistencePort
                .getAllCategories()
                .buildIdListWithSubcategories(UUID.fromString(filterCategory))
                .map { it.id.toString() }
        } ?: emptyList()



        return itemPersistencePort.getAllItems()
            .asSequence()
            .filter { it.name.contains(filterItemsCommand.termFilter ?: "", ignoreCase = true) }
            .filter {
                filterItemsCommand.categoryIdFilter?.let { filter ->
                    if (filter == EMPTY_CATEGORY.toString()) true
                    else it.category?.id?.toString() in categoryIdsForFilteringList
                } ?: true
            }
            .filter { it.value < (filterItemsCommand.lowerThanValueFilter?.toDouble() ?: Double.MAX_VALUE) }
            .filter { it.value > (filterItemsCommand.greaterThanValueFilter?.toDouble() ?: Double.MIN_VALUE) }
            .filter { it.amount < (filterItemsCommand.lowerThanAmountFilter?.toDouble() ?: Double.MAX_VALUE) }
            .filter { it.amount > (filterItemsCommand.greaterThanAmountFilter?.toDouble() ?: Double.MIN_VALUE) }
            .toList()
            .map { it.toProjection() }
    }

    override fun logStackTrace(logStackTraceCommand: LogStackTraceCommand) {
        logPersistencePort.createLog(LogDto(log= logStackTraceCommand.stackTrace, username = logStackTraceCommand.username, timeStamp = LocalDateTime.now()))
    }

    override fun getItem(command: GetItemCommand): ItemProjection {
        return itemPersistencePort.getItem(command.id).toProjection()
    }

    override fun createNewItem(command: CreateItemCommand) {
        ItemDto(
            name = command.name,
            amount = command.amount,
            value = command.value,
            category = categoryPersistencePort.getCategoryById(UUID.fromString(command.category))
        ).also {
            it.validate(itemPersistencePort)
            itemPersistencePort.saveItem(it)
        }
    }

    override fun editItem(command: EditItemCommand) {
        ItemDto(
            id = command.id,
            name = command.name,
            amount = command.amount,
            value = command.value,
            category = categoryPersistencePort.getCategoryById(UUID.fromString(command.category))
        ).also {
            it.validate()
            itemPersistencePort.saveItem(it)
        }
    }

    override fun deleteItem(command: DeleteItemCommand) {
        itemPersistencePort.deleteItem(command.id)
    }

    override fun getAllUsers() : List<UserProjection> =
        authorityPersistencePort.getAllAuthorities().map { it.toProjection() }

    override fun registerUser(command: RegisterUserCommand) {
        if (userDetailsManager.userExists(command.username)) throw Exception("User already exists")
        val user = User.builder()
            .username(command.username)
            .password(passwordEncoder.encode(command.password))
            .roles(command.role.name)
            .build()
        userDetailsManager.createUser(user)
    }

}