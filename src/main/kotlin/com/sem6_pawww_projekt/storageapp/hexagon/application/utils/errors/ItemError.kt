package com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.ItemPersistencePort

sealed class ItemError : Exception() {
    object ItemExists : ItemError()
    object ValueIncorrect : ItemError()
    object AmountIncorrect : ItemError()
    data class Errors(val errors: List<ItemError>) : ItemError()
}

/**
 * @param itemPersistencePort pass persistence port to validate if item with the same name exists in DB
 */
fun ItemDto.validate(itemPersistencePort: ItemPersistencePort? = null){
    val errors = mutableListOf<ItemError>()

    // validate
    itemPersistencePort?.getAllItems()
        ?.find { it.name == name }
        ?.let { errors.add(ItemError.ItemExists) }
    if (amount <= 0) errors.add(ItemError.AmountIncorrect)
    if (value <= 0) errors.add(ItemError.ValueIncorrect)

    // throw errors if needed
    if (errors.size > 0) throw ItemError.Errors(errors)
}