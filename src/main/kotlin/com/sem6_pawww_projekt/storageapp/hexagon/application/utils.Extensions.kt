package com.sem6_pawww_projekt.storageapp.hexagon.application

import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import java.util.*

fun List<CategoryDto>.buildIdListWithSubcategories(id: UUID): List<CategoryDto> =
    mutableListOf<CategoryDto>()
        .apply {
            this@buildIdListWithSubcategories.find { it.id == id }?.let { add(0, it) }
            addAll(this@buildIdListWithSubcategories.getSubcategories(id))
        }.toList()

private fun List<CategoryDto>.getSubcategories(id: UUID): List<CategoryDto> =
    mutableListOf<CategoryDto>()
        .apply {
            this@getSubcategories.filter { it.masterCategory?.id == id }
                .forEach {
                    add(it)
                    addAll(this@getSubcategories.getSubcategories(it.id))
                }
        }
        .distinct()