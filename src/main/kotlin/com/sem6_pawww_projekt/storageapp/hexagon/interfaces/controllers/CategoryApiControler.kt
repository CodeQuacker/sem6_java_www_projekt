package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.CreateCategoryCommand
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.Response
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.toResponse
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryTreeProjection
import io.swagger.v3.oas.annotations.Operation
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/category")
class CategoryApiControler(private val storageAppService: StorageAppInterface) {

    @GetMapping("/getAllCategories")
    @Operation(tags = ["Category"], summary = "Get all categories")
    fun getAllCategories(): Response<List<CategoryProjection>> {
        return storageAppService.getAllCategories().toResponse(HttpStatus.OK)
    }

    @PostMapping("/addCategory")
    @Operation(tags = ["Category"], summary = "Add category")
    fun addCategory(@RequestBody command: CreateCategoryCommand): Response<Unit> {
        return storageAppService.createNewCategory(command).toResponse(HttpStatus.OK)
    }
}
