package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.LogStackTraceCommand
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import jakarta.servlet.http.HttpSession
import org.slf4j.LoggerFactory



class StackTraceHandlers {
    val logger = LoggerFactory.getLogger(StackTraceHandlers::class.java)
    fun startStackTrace(session: HttpSession,username: String){
        logger.info("Starting stacktrace for user $username")
        session.setAttribute("stackTrace", "Login($username)->")
    }

    fun updateStackTrace(session: HttpSession,message:String){
        val stacktrace = session.getAttribute("stackTrace")
        logger.info("Actual stacktrace: $stacktrace")
        logger.info("Updating stacktrace with message $message")
        session.setAttribute("stackTrace", "$stacktrace$message->")
    }

    fun saveStackTrace(session: HttpSession, storageAppInterface: StorageAppInterface,username: String){
        val stacktrace = session.getAttribute("stackTrace")
        logger.info("Saving stacktrace: $stacktrace")
        storageAppInterface.logStackTrace(LogStackTraceCommand(stacktrace.toString(),username))
        session.removeAttribute("stackTrace")
    }
}

fun startStackTrace(session: HttpSession,username: String){
    StackTraceHandlers().startStackTrace(session,username)
}

fun updateStackTrace(session: HttpSession,message:String){
    StackTraceHandlers().updateStackTrace(session,message)
}

fun saveStackTrace(session: HttpSession, storageAppInterface: StorageAppInterface,username: String){
    StackTraceHandlers().saveStackTrace(session,storageAppInterface,username)
}