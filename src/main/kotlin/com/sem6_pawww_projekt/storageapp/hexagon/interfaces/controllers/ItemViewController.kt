package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors.ItemError
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.updateStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.*
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import jakarta.servlet.http.Cookie
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.EMPTY_CATEGORY
import jakarta.servlet.http.HttpSession
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import java.util.*


@Controller
@RequestMapping("/items")
class ItemViewController(
    private val storageAppService: StorageAppInterface
) {
    @GetMapping("")
    fun items(
        model: Model,
        request: HttpServletRequest,
        response: HttpServletResponse,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Go to items list")
        val itemTermFilter = request.cookies?.find { it.name == "ItemTermFilter" }?.value.also {
            createCookieAndAddValueToModel(
                name = "ItemTermFilter",
                value = it?:"",
                age = 60 * 60 * 24 * 30,
                response = response,
                model = model,
            )
        }
        val itemCategoryNameFilter = request.cookies?.find { it.name == "ItemCategoryNameFilter" }?.value.also {
            createCookieAndAddValueToModel(
                name = "ItemCategoryNameFilter",
                value = it?:"",
                age = 60 * 60 * 24 * 30,
                response = response,
                model = model,
            )
        }
        val itemLowerThanValueFilter =
            request.cookies?.find { it.name == "ItemLowerThanValueFilter" }?.value?.toDoubleOrNull()
                .also { createCookieAndAddValueToModel(
                    name = "ItemLowerThanValueFilter",
                    value = it?.toString()?:"",
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                ) }
        val itemGreaterThanValueFilter =
            request.cookies?.find { it.name == "ItemGreaterThanValueFilter" }?.value?.toDoubleOrNull()
                .also { createCookieAndAddValueToModel(
                    name = "ItemGreaterThanValueFilter",
                    value = it?.toString()?:"",
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                ) }
        val itemLowerThanAmountFilter =
            request.cookies?.find { it.name == "ItemLowerThanAmountFilter" }?.value?.toIntOrNull()
                .also { createCookieAndAddValueToModel(
                    name = "ItemLowerThanAmountFilter",
                    value = it?.toString()?:"",
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                ) }
        val itemGreaterThanAmountFilter =
            request.cookies?.find { it.name == "ItemGreaterThanAmountFilter" }?.value?.toIntOrNull()
                .also { createCookieAndAddValueToModel(
                    name = "ItemGreaterThanAmountFilter",
                    value = it?.toString()?:"",
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                ) }

        storageAppService.getAllCategories()
            .toMutableList()
            .also { it.add(0, CategoryProjection(EMPTY_CATEGORY, "", "")) }
            .let { model.addAttribute("categories", it) }

        storageAppService.getAllFilteredItems(
            FilterItemsCommand(
                termFilter = itemTermFilter,
                categoryIdFilter = itemCategoryNameFilter,
                lowerThanValueFilter = itemLowerThanValueFilter,
                greaterThanValueFilter = itemGreaterThanValueFilter,
                lowerThanAmountFilter = itemLowerThanAmountFilter,
                greaterThanAmountFilter = itemGreaterThanAmountFilter
            )
        ).sortedBy {
            it.name
        }.let {
            model.addAttribute("itemList", it)
        }

        return "items"
    }

    @PostMapping("")
    fun filterItems(
        model: Model,
        response: HttpServletResponse,
        session: HttpSession,
        @RequestParam("termFilter") termFilter: String?,
        @RequestParam("categoryNameFilter") categoryNameFilter: String?,
        @RequestParam("lowerThanValueFilter") lowerThanValueFilter: Number?,
        @RequestParam("greaterThanValueFilter") greaterThanValueFilter: Number?,
        @RequestParam("lowerThanAmountFilter") lowerThanAmountFilter: Number?,
        @RequestParam("greaterThanAmountFilter") greaterThanAmountFilter: Number?,
    ): String {
        updateStackTrace(session,"Go to filtered items list")
        val newItemFilterCookie = FilterItemsCommand(
            termFilter = termFilter,
            categoryIdFilter = categoryNameFilter,
            lowerThanValueFilter = lowerThanValueFilter,
            greaterThanValueFilter = greaterThanValueFilter,
            lowerThanAmountFilter = lowerThanAmountFilter,
            greaterThanAmountFilter = greaterThanAmountFilter
        )

        storageAppService.getAllCategories()
            .toMutableList()
            .also { it.add(0, CategoryProjection(EMPTY_CATEGORY, "", "")) }
            .let { model.addAttribute("categories", it) }

        storageAppService.getAllFilteredItems(
            newItemFilterCookie
        ).sortedBy {
            it.name
        }.also {
            if (termFilter != null && termFilter != "") {
                createCookieAndAddValueToModel(
                    name = "ItemTermFilter",
                    value = termFilter,
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            if (categoryNameFilter != null && categoryNameFilter != "") {
                createCookieAndAddValueToModel(
                    name = "ItemCategoryNameFilter",
                    value = categoryNameFilter,
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            if (lowerThanValueFilter != null) {
                createCookieAndAddValueToModel(
                    name = "ItemLowerThanValueFilter",
                    value = lowerThanValueFilter.toString(),
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            if (greaterThanValueFilter != null) {
                createCookieAndAddValueToModel(
                    name = "ItemGreaterThanValueFilter",
                    value = greaterThanValueFilter.toString(),
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            if (lowerThanAmountFilter != null) {
                createCookieAndAddValueToModel(
                    name = "ItemLowerThanAmountFilter",
                    value = lowerThanAmountFilter.toString(),
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            if (greaterThanAmountFilter != null) {
                createCookieAndAddValueToModel(
                    name = "ItemGreaterThanAmountFilter",
                    value = greaterThanAmountFilter.toString(),
                    age = 60 * 60 * 24 * 30,
                    response = response,
                    model = model,
                )
            }
            model.addAttribute("itemList", it)
        }

        return "items"
    }

    @GetMapping("/clearFilter")
    fun clearItemFilters(response: HttpServletResponse, model: Model, session: HttpSession): String {
        updateStackTrace(session,"Clear item filters")
        createCookieAndAddValueToModel(
            name = "ItemTermFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )
        createCookieAndAddValueToModel(
            name = "ItemCategoryNameFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )
        createCookieAndAddValueToModel(
            name = "ItemLowerThanValueFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )
        createCookieAndAddValueToModel(
            name = "ItemGreaterThanValueFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )
        createCookieAndAddValueToModel(
            name = "ItemLowerThanAmountFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )
        createCookieAndAddValueToModel(
            name = "ItemGreaterThanAmountFilter",
            value = null,
            age = 0,
            response = response,
            model = model,
        )

        storageAppService.getAllCategories().toMutableList()
            .also { it.add(0, CategoryProjection(UUID.randomUUID(), "", "")) }.let {
                model.addAttribute("categories", it)
            }
        storageAppService.getAllItems().sortedBy {
            it.name
        }.also {
            model.addAttribute("itemList", it)
        }

        return "items"
    }

    @GetMapping("/details/{id}")
    fun item(
        @PathVariable("id") itemId: UUID,
        model: Model,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Go to item($itemId) details")
        storageAppService.getItem(GetItemCommand(itemId)).also {
            model.addAttribute("item", it)
        }

        return "item"
    }

    @GetMapping("/delete/{id}")
    fun deleteItem(
        @PathVariable("id") itemId: UUID,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Delete item($itemId)")
        storageAppService.deleteItem(DeleteItemCommand(itemId))

        return "redirect:/items"
    }

    @GetMapping("/edit/{id}")
    fun editItem(
        @PathVariable("id") itemId: UUID,
        model: Model,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Go to edit item($itemId)")
        storageAppService.getAllCategories()
            .let { model.addAttribute("categories", it) }
        storageAppService.getItem(GetItemCommand(itemId)).run {
            model.addAttribute("id", id)
            model.addAttribute("name", name)
            model.addAttribute("value", value)
            model.addAttribute("amount", amount)
            model.addAttribute("category", category)
        }

        return "itemEdit"
    }

    @PostMapping("/edit/{id}")
    fun editItemPost(
        @PathVariable("id") itemId: UUID,
        @RequestParam("name") name: String,
        @RequestParam("value") value: Double,
        @RequestParam("amount") amount: Int,
        @RequestParam("category") category: String,
        model: Model,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Edit item($itemId)")
        EditItemCommand(
            id = itemId,
            name = name,
            value = value,
            amount = amount,
            category = category,
        ).also {
            runCatching {
                storageAppService.editItem(it)
            }.onFailure { throwable ->
                if (throwable is ItemError.Errors) {
                    throwable.errors.forEach { error ->
                        when (error) {
                            ItemError.ValueIncorrect ->
                                model.addAttribute("valueIncorrect", "Value of item can't be less than or equal 0")

                            ItemError.AmountIncorrect ->
                                model.addAttribute("amountIncorrect", "Amount of item can't be less than or equal 0")

                            else -> throw error
                        }
                    }
                }
            }.onSuccess { return "redirect:/items/details/$itemId" }
        }

        model.addAttribute("id", itemId)
        model.addAttribute("name", name)
        model.addAttribute("value", value)
        model.addAttribute("amount", amount)
        model.addAttribute("category", category)

        return "itemEdit"
    }

    @GetMapping("/add")
    fun addItem(model:Model,session: HttpSession): String {
        updateStackTrace(session,"Go to add item")
        storageAppService.getAllCategories()
            .let { model.addAttribute("categories", it) }
        return "itemAdd"
    }

    @PostMapping("/add")
    fun addItemPost(
        @RequestParam("name") name: String,
        @RequestParam("value") value: Double,
        @RequestParam("amount") amount: Int,
        @RequestParam("category") category: String,
        model: Model,session: HttpSession
    ): String {
        updateStackTrace(session,"Add item")
        CreateItemCommand(
            name = name,
            value = value,
            amount = amount,
            category = category
        ).also {
            runCatching {
                storageAppService.createNewItem(it)
            }.onFailure { throwable ->
                if (throwable is ItemError.Errors) {
                    throwable.errors.forEach { error ->
                        when (error) {
                            ItemError.ItemExists ->
                                model.addAttribute("itemExists", "Item with this name already exists")

                            ItemError.ValueIncorrect ->
                                model.addAttribute("valueIncorrect", "Value of item can't be less than or equal 0")

                            ItemError.AmountIncorrect ->
                                model.addAttribute("amountIncorrect", "Amount of item can't be less than or equal 0")

                            else -> throw error
                        }
                    }
                }
            }.onSuccess {
                return "redirect:/items"
            }
        }

        model.addAttribute("name", name)
        model.addAttribute("value", value)
        model.addAttribute("amount", amount)
        model.addAttribute("category", category)

        return "itemAdd"
    }

    private fun createCookieAndAddValueToModel(
        name: String,
        value: String?,
        age: Int,
        response: HttpServletResponse,
        model: Model
    ) {
        response.addCookie(Cookie(name, value).apply { maxAge = age })
        model.addAttribute(name, value)
    }
}