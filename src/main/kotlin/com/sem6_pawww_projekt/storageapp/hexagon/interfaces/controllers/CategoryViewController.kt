package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.application.utils.errors.CategoryError
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.updateStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.CreateCategoryCommand
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.DeleteCategoryCommand
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.CategoryProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.EMPTY_CATEGORY
import jakarta.servlet.http.HttpSession
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.util.*


@Controller
@RequestMapping("/categories")
class CategoryViewController(
    private val storageAppService: StorageAppInterface
) {
    @GetMapping("")
    fun categories(
        model: Model,
        session: HttpSession
    ): String {
        updateStackTrace(session, "Got to categories page")
        storageAppService.getAllCategories().filter { it.masterCategory == null || it.masterCategory == "" }
            .toMutableList()
            .also { it.add(0, CategoryProjection(EMPTY_CATEGORY, "", "")) }
            .let { model.addAttribute("categoriesForm", it) }
        storageAppService.getAllCategories().let {
            model.addAttribute("categoryList", it)
        }

        return "categories"
    }

    @PostMapping("/add")
    @PreAuthorize("hasAnyRole('ADMIN','FULL_USER')")
    fun addCategoryPost(
        @RequestParam("name") name: String,
        @RequestParam("masterCategory") masterCategory: String?,
        model: Model, session: HttpSession
    ): String {
        updateStackTrace(session, "Adding category")
        runCatching {
            CreateCategoryCommand(name, masterCategory).let {
                storageAppService.createNewCategory(it)
            }
        }.onFailure {
            when (it) {
                is CategoryError.CategoryExists -> {
                    storageAppService.getAllCategories().let { categoryList ->
                        model.addAttribute("categoryList", categoryList)
                        model.addAttribute("error", "Category with that name already exists")
                    }
                    return "categories"
                }

                else -> {}
            }
        }

        return "redirect:/categories"
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','FULL_USER')")
    fun deleteCategory(
        @PathVariable("id") categoryId: UUID,
        session: HttpSession
    ): String {
        updateStackTrace(session, "Deleting category")
        storageAppService.deleteCategory(DeleteCategoryCommand(categoryId))
        return "redirect:/categories"
    }


}