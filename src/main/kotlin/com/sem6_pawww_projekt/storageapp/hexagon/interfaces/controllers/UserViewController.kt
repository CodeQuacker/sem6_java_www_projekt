package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.updateStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.RegisterUserCommand
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserRoles
import jakarta.servlet.http.HttpSession
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/user")
@PreAuthorize("hasRole('ADMIN')")
class UserViewController(private val storageAppService: StorageAppInterface) {

    @GetMapping("/register")
    @PreAuthorize("hasRole('ADMIN')")
    fun registerGet(session: HttpSession): String {
        updateStackTrace(session,"Got to register page")
        return "register"
    }

    @PostMapping("/register")
    fun registerPost(
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("role") role: String,
        model: Model,
        session: HttpSession
    ): String {
        updateStackTrace(session,"Registering user")
        storageAppService.registerUser(RegisterUserCommand(username, password, UserRoles.valueOf(role)))
        model.addAttribute("username", username)
        model.addAttribute("password", password)
        model.addAttribute("role", role)
        return "redirect:register"
    }
}