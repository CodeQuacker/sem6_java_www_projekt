package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.commands.RegisterUserCommand
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserProjection
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.objects.UserRoles
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam


@Controller
@RequestMapping("/admin")
class AdminViewController(
    private val storageAppService: StorageAppInterface
) {
    @GetMapping("")
    fun adminGet(
        model: Model
    ): String {
        val roles = UserRoles.values().toList()
        val users = storageAppService.getAllUsers()

        model.addAttribute("roles", roles)
        model.addAttribute("users", users)

        return "adminUsersList"
    }

    @PostMapping("")
    fun adminPost(
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("role") role: UserRoles,
        model: Model
    ): String {
        runCatching {
            check(username.isNotBlank() && password.isNotBlank())
        }.onFailure {
            println(it.stackTraceToString())
        }.onSuccess {
            println("adding user")
            storageAppService.registerUser(RegisterUserCommand(
                username = username,
                password = password,
                role = role
            ))
        }

        return "redirect:/admin"
    }
}