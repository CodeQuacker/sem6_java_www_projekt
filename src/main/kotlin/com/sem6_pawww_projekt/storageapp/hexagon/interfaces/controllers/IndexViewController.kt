package com.sem6_pawww_projekt.storageapp.hexagon.interfaces.controllers

import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.saveStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.startStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.updateStackTrace
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import jakarta.servlet.http.HttpSession
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.security.core.Authentication
import org.springframework.security.core.annotation.CurrentSecurityContext
import org.springframework.security.core.context.SecurityContextHolder


@Controller
class IndexViewController(private val storageAppInterface: StorageAppInterface) {
    @GetMapping("/")
    fun index(
        session: HttpSession
    ): String {
        updateStackTrace(session,"Go to index")
        return "redirect:/items"
    }

    @GetMapping("/login")
    fun loginGet(): String {
        return "index"
    }

    @PostMapping("/login")
    fun login(
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        model: Model,
        session: HttpSession
    ): String {
        startStackTrace(session, username)
        return "redirect:/items"
    }
    @GetMapping("/logout")
    fun logout(
        session: HttpSession,
        @CurrentSecurityContext(expression="authentication.name") username: String
    ): String {
        saveStackTrace(session, storageAppInterface, username)
        return "redirect:/login"
    }
}