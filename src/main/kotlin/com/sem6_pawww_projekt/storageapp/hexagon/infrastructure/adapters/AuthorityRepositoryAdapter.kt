package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.AuthorityRepository
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.AuthorityDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.AuthorityPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.utils.toDto

class AuthorityRepositoryAdapter(private val authorityRepository: AuthorityRepository) : AuthorityPersistencePort {
    override fun getAllAuthorities(): List<AuthorityDto> {
        return authorityRepository.findAll().toList().map { it.toDto() }
    }
}