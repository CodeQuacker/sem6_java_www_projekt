package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.LogEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.LogDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.LogDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.LogPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.utils.toDto

class LogRepositoryAdapter(private val logDBRepository: LogDBRepository): LogPersistencePort {
    override fun createLog(logDto: LogDto) {
        val log = LogEntity().also {
            it.log = logDto.log
            it.username = logDto.username
        }.let { logDBRepository.save(it) }
    }

    override fun getLogs(): List<LogDto> {
        return logDBRepository.findAll().map { it.toDto() }
    }
}