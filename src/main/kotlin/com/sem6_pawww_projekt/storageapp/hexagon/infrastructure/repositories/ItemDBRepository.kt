package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.ItemEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ItemDBRepository : JpaRepository<ItemEntity, UUID>, JpaSpecificationExecutor<ItemEntity>, CrudRepository<ItemEntity, UUID>{

    @Modifying
    @Query(value = "DELETE FROM public.item_entity WHERE id = :id", nativeQuery = true)
    override fun deleteById(@Param("id") id: UUID)

}