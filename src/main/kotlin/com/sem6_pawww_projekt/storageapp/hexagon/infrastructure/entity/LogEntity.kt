package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity

import jakarta.persistence.*
import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDateTime
import java.util.*

@Entity
@Table(name = "log_entity")
open class LogEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Lob
    @Column(name = "log", nullable = false)
    open var log: String? = null

    @Column(name = "username", nullable = false)
    open var username: String? = null

    @CreationTimestamp
    @Column(name = "time_stamp", nullable = false)
    open var timeStamp: LocalDateTime? = null
}