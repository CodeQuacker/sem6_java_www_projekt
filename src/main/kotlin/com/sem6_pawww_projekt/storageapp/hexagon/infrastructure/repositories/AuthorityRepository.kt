package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories;

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.AuthorityEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.UserEntity
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository : CrudRepository<AuthorityEntity, UserEntity> {
}