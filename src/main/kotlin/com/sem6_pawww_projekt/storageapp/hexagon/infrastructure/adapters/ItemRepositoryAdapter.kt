package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.ItemEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.CategoryDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.ItemDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.ItemDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.ItemPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.utils.toDto
import java.util.*

class ItemRepositoryAdapter(
    private val itemRepository: ItemDBRepository,
    private val categoryRepository: CategoryDBRepository
): ItemPersistencePort{
    override fun getItem(id: UUID): ItemDto = itemRepository.getReferenceById(id).toDto()

    override fun getAllItems(): List<ItemDto> = itemRepository.findAll().map { it.toDto() }

    override fun saveItem(item: ItemDto){
        ItemEntity().also {
            it.id = item.id
            it.name = item.name
            it.value = item.value
            it.amount = item.amount
            it.category = item.category?.id?.let { uuid -> categoryRepository.getReferenceById(uuid) }
        }.let {
            itemRepository.save(it)
        }
    }

    override fun deleteItem(id: UUID) {
        itemRepository.deleteById(id)
    }
}