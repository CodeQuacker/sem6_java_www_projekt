package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "category_entity")
open class CategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "name", nullable = false, unique = true)
    open var name: String? = null

    @ManyToOne
    @JoinColumn(name = "master_category_id")
    open var masterCategory: CategoryEntity? = null
}