package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "item_entity")
open class ItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "name", nullable = false, unique = true)
    open var name: String? = null

    @Column(name = "value", nullable = false)
    open var value: Double? = null

    @Column(name = "amount")
    open var amount: Int? = null

    @ManyToOne(cascade = [CascadeType.ALL], optional = false)
    @JoinColumn(name = "category_id", nullable = false)
    open var category: CategoryEntity? = null
}