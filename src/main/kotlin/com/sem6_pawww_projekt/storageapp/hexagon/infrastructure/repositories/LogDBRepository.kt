package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories;

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.LogEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import java.util.*

interface LogDBRepository : JpaRepository<LogEntity, UUID>, JpaSpecificationExecutor<LogEntity>, CrudRepository<LogEntity, UUID> {

}