package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size
import java.io.Serializable

@Entity
@Table(name = "authorities")
open class AuthorityEntity {
    @EmbeddedId
    open var id: AuthorityEntityId? = null

    @Size(max = 50)
    @NotNull
    @Column(name = "authority", nullable = false, length = 50)
    open var authority: String? = null
}

@Embeddable
class AuthorityEntityId : Serializable {
    @ManyToOne(optional = false)
    @JoinColumn(name = "username", nullable = false)
    var username: UserEntity? = null
}