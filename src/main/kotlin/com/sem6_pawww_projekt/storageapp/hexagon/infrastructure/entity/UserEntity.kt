package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size

@Entity
@Table(name = "users")
open class UserEntity {
    @Id
    @Size(max = 50)
    @Column(name = "username", nullable = false, length = 50)
    open var username: String? = null

    @Size(max = 500)
    @NotNull
    @Column(name = "password", nullable = false, length = 500)
    open var password: String? = null

    @NotNull
    @Column(name = "enabled", nullable = false)
    open var enabled: Boolean? = false
}