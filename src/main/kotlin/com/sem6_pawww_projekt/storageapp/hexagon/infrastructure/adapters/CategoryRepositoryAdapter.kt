package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.CategoryEntity
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.CategoryDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.objects.CategoryDto
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.CategoryPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.utils.toDto
import java.util.*

class CategoryRepositoryAdapter(
    private val categoryRepository: CategoryDBRepository
): CategoryPersistencePort{
    override fun createMasterCategory(name: String) {
        val category = CategoryEntity()
        category.name = name
        categoryRepository.save(category)
    }

    override fun createSubCategory(name: String, masterCategoryName: String) {
        val category = CategoryEntity()
        category.name = name
        category.masterCategory = categoryRepository.findByName(masterCategoryName).get()
        categoryRepository.save(category)
    }

    override fun getAllCategories(): List<CategoryDto> {
        return categoryRepository.findAll().map { it.toDto() }
    }

    override fun getMasterCategories(): List<CategoryDto> {
        return categoryRepository.findByNameAndMasterCategoryNull("").map { it.toDto() }
    }

    override fun getSubCategories(): List<CategoryDto> {
        return categoryRepository.findByNameAndMasterCategoryNotNull("").map { it.toDto() }
    }

    override fun getSubCategories(masterCategoryName: String): List<CategoryDto> {
        return categoryRepository.findAll().filter { it.masterCategory?.name == masterCategoryName }.map { it.toDto()}
    }

    override fun deleteCategory(id: UUID) {
        categoryRepository.deleteById(id)
    }

    override fun existsCategory(name: String): Boolean {
        return categoryRepository.existsByName(name)
    }

    override fun getCategory(name: String): CategoryDto {
        return categoryRepository.findByName(name).get().toDto()
    }

    override fun getCategoryById(id: UUID): CategoryDto {
        return categoryRepository.findById(id).get().toDto()
    }

    override fun updateCategory(name: String, newName: String) {
        val category = categoryRepository.findByName(name).get()
        category.name = newName
        categoryRepository.save(category)
    }

    override fun updateCategory(name: String, newName: String, masterCategoryName: String) {
        val category = categoryRepository.findByName(name).get()
        category.name = newName
        category.masterCategory = categoryRepository.findByName(masterCategoryName).get()
        categoryRepository.save(category)
    }

}