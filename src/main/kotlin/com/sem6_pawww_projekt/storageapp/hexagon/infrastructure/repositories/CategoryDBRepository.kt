package com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories

import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.entity.CategoryEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CategoryDBRepository : JpaRepository<CategoryEntity, UUID>, JpaSpecificationExecutor<CategoryEntity>, CrudRepository<CategoryEntity, UUID> {

    fun findByName(name: String): Optional<CategoryEntity>

    fun findByNameAndMasterCategoryNull(name: String): List<CategoryEntity>

    fun findByNameAndMasterCategoryNotNull(name: String): List<CategoryEntity>

    fun findByNameAndMasterCategoryNotNullAndMasterCategoryName(name: String, masterCategoryName: String): List<CategoryEntity>

    fun existsByName(name: String): Boolean

    fun deleteByName(name: String): Long
}