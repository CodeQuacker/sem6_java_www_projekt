package com.sem6_pawww_projekt.storageapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StorageAppApplication

fun main(args: Array<String>) {
    runApplication<StorageAppApplication>(*args)
}
