package com.sem6_pawww_projekt.storageapp.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.servers.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfiguration {
    @Bean
    fun openApi(
        @Value("\${server.servlet.context-path}") contextPath: String,
    ): OpenAPI =
        OpenAPI().addServersItem(Server().url(contextPath)).addServersItem(Server().url("$contextPath/api"))
}