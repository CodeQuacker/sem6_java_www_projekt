package com.sem6_pawww_projekt.storageapp.config

import com.sem6_pawww_projekt.storageapp.hexagon.application.StorageAppService
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters.AuthorityRepositoryAdapter
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters.CategoryRepositoryAdapter
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters.ItemRepositoryAdapter
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.adapters.LogRepositoryAdapter
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.AuthorityRepository
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.CategoryDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.ItemDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.infrastructure.repositories.LogDBRepository
import com.sem6_pawww_projekt.storageapp.hexagon.interfaces.utils.StackTraceHandlers
import com.sem6_pawww_projekt.storageapp.hexagon.ports.input.interfaces.StorageAppInterface
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.AuthorityPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.CategoryPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.ItemPersistencePort
import com.sem6_pawww_projekt.storageapp.hexagon.ports.output.repositories.LogPersistencePort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.provisioning.UserDetailsManager

@Configuration
class BeanConfiguration {

    @Bean
    fun storageAppServiceBean(
        categoryPersistencePort: CategoryPersistencePort,
        itemPersistencePort: ItemPersistencePort,
        userDetailsManager: UserDetailsManager,
        passwordEncoder: PasswordEncoder,
        logPersistencePort: LogPersistencePort,
        authorityPersistencePort: AuthorityPersistencePort
    ): StorageAppInterface =
        StorageAppService(
            categoryPersistencePort = categoryPersistencePort,
            itemPersistencePort = itemPersistencePort,
            userDetailsManager = userDetailsManager,
            passwordEncoder = passwordEncoder,
            logPersistencePort = logPersistencePort,
            authorityPersistencePort = authorityPersistencePort
        )

    @Bean
    fun categoryRepositoryAdapterBean(categoryRepository: CategoryDBRepository): CategoryPersistencePort =
        CategoryRepositoryAdapter(categoryRepository = categoryRepository)

    @Bean
    fun itemRepositoryAdapterBean(
        itemRepository: ItemDBRepository,
        categoryRepository: CategoryDBRepository
    ): ItemPersistencePort = ItemRepositoryAdapter(itemRepository, categoryRepository)

    @Bean
    fun logPersistencePortBean(logDBRepository: LogDBRepository): LogPersistencePort =
        LogRepositoryAdapter(logDBRepository = logDBRepository)

    @Bean
    fun stackTraceHandlerBean(): StackTraceHandlers = StackTraceHandlers()

    @Bean
    fun authorityPersistencePortBean(authorityRepository: AuthorityRepository): AuthorityPersistencePort =
        AuthorityRepositoryAdapter(authorityRepository = authorityRepository)
}