package com.sem6_pawww_projekt.storageapp.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.provisioning.JdbcUserDetailsManager
import org.springframework.security.provisioning.UserDetailsManager
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.logout.LogoutHandler
import javax.sql.DataSource
import kotlin.jvm.Throws

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
class SecurityConfig {

    @Bean
    @Throws(Exception::class)
    fun securityFilterChainBean(http: HttpSecurity): SecurityFilterChain {
        http
            .csrf()
            .and()
            .cors()
            .and()
            .authorizeHttpRequests { requests ->
                requests.anyRequest().authenticated()
            }.headers { it.frameOptions().sameOrigin() }
            .formLogin(Customizer.withDefaults())
            .logout().logoutUrl("/logout").invalidateHttpSession(true).and()
            .sessionManagement().invalidSessionUrl("/login")

        return http.build()
    }


    @Bean
    fun authenticationProviderBean(
        @Qualifier("userDetailsService") userDetailsService: UserDetailsService,
        passwordEncoder: PasswordEncoder
    ): DaoAuthenticationProvider {
        val authProvider = DaoAuthenticationProvider()
        authProvider.setUserDetailsService(userDetailsService)
        authProvider.setPasswordEncoder(passwordEncoder)
        return authProvider
    }

    @Bean
    @Qualifier("userDetailsService")
    fun userDetailsServiceBean(
        dataSource: DataSource,
        passwordEncoder: PasswordEncoder,
        userDetailsManager: UserDetailsManager
    ): UserDetailsService {
        val user = User.builder()
            .username("admin")
            .password(passwordEncoder.encode("admin"))
            .roles("ADMIN")
            .build()
        when {
            !userDetailsManager.userExists(user.username) -> userDetailsManager.createUser(user)
        }
        return userDetailsManager
    }

    @Bean
    fun userDetailsManagerBean(dataSource: DataSource): JdbcUserDetailsManager = JdbcUserDetailsManager(dataSource)

    @Bean
    fun passwordEncoderBean(): PasswordEncoder = BCryptPasswordEncoder()

}