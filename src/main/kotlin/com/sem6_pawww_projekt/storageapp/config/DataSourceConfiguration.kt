package com.sem6_pawww_projekt.storageapp.config

import liquibase.integration.spring.SpringLiquibase
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class DataSourceConfiguration {
    @Bean
    fun dataSourceBean(
        @Value("\${spring.datasource.driver-class-name}") driverClassName: String?,
        @Value("\${spring.datasource.url}") url: String?,
        @Value("\${spring.datasource.username}") username: String?,
        @Value("\${spring.datasource.password}") password: String?
    ): DataSource {
        val dataSource = DataSourceBuilder.create()
        dataSource.driverClassName(driverClassName)
        dataSource.url(url)
        dataSource.username(username)
        dataSource.password(password)
        return dataSource.build()
    }

    @Bean
    fun liquibaseBean(dataSource: DataSource,@Value("\${spring.liquibase.change-log}") changeLog: String): SpringLiquibase {
        val liquibase = SpringLiquibase()
        liquibase.dataSource = dataSource
        liquibase.changeLog = changeLog
        return liquibase
    }
}